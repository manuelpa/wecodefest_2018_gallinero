export default [
  {
    name: 'app-greet',
    module: () => import('./Greet.vue'),
  },
  {
    name: 'app-header',
    module: () => import('./AppHeader.vue'),
  },
  {
    name: 'app-nav',
    module: () => import('./AppNav.vue'),
  },
];
