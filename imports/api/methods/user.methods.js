import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';
import rateLimiter from '../../lib/rateLimiter';

const userMethods = {
  'users.createUser'({ username, email, password }, profile) {
    check(username, String);
    check(email, String);
    check(password, String);
    check(profile, Object);

    return Accounts.createUser({ username, email, password, ...profile });
  },
};

Meteor.methods(userMethods);
rateLimiter(Object.keys(userMethods));
