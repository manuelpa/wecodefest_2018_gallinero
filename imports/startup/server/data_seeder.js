/* eslint-disable no-unused-vars, no-undef */
import { Meteor } from 'meteor/meteor';
import faker from 'faker';

// ======== EDIT THESE LINES ========
// uncomment if needed
// import Collection from '../../api/collections/anyCollection.collection';

// this function must return an object according to Collection model
const collectionModel = (userId, index) => null;

const COLLECTION_AMOUNT = 0;
const REGULAR_USERS_AMOUNT = 0;
// ======== END EDITABLE CONTENT ========


if (!Meteor.isDevelopment) return;
if (Meteor.users.find().count()) return;

const DEGREDILLA = {
  username: 'degredilla',
  password: '123123',
  profile: {
    avatar: '/images/degredilla.png',
    name: {
      first: 'Manuel',
      last: 'Peña',
    },
  },
};

const initCollection = (userId) => {
  if (!Collection || !COLLECTION_AMOUNT || !collectionModel()) return;

  for (let i = 0; i < COLLECTION_AMOUNT; i += 1) {
    Collection.insert(collectionModel(userId, i));
  }
};
const initUser = (user, callback) => {
  const userId = Accounts.createUser(user);
  callback(userId);
};


initUser(DEGREDILLA, (userId) => {
  Accounts.addEmail(userId, 'manuelpa@openmailbox.org', true);
  if (COLLECTION_AMOUNT && !!collectionModel()) initCollection(userId);
});

if (REGULAR_USERS_AMOUNT) {
  for (let i = 0; i < REGULAR_USERS_AMOUNT; i += 1) {
    initUser(
      {
        username: faker.internet.userName(),
        email: `user_${i + 1}@fake.com`,
        password: '123123',
        profile: {
          avatar: '/images/defaultAvatar.png',
          name: {
            first: faker.name.firstName(),
            last: faker.name.lastName(),
          },
        },
      },
      (userId) => {
        if (COLLECTION_AMOUNT) initCollection(userId);
      },
    );
  }
}
