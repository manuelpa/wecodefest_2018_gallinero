import { Meteor } from 'meteor/meteor';

const ACCOUNT_ROUTES = [
  'signup',
  'login',
];
const PUBLIC_ROUTES = [
  ...ACCOUNT_ROUTES,
];

const notFoundredirectedToRoot = (to, from, next) => {
  if (!to.name) next('/');
  next();
};
const loggedUserCannotVisitAccountRoutes = (to, from, next) => {
  if (!ACCOUNT_ROUTES.some(route => route === to.name)) {
    next();
    return;
  }
  if (Meteor.userId()) {
    next('/');
    return;
  }
  next();
};
const userMustBeLogged = (to, from, next) => {
  if (PUBLIC_ROUTES.some(route => route === to.name)) {
    next();
    return;
  }

  if (!Meteor.userId()) {
    next('/login');
    return;
  }

  next();
};

export default [
  notFoundredirectedToRoot,
  loggedUserCannotVisitAccountRoutes,
  userMustBeLogged,
];
