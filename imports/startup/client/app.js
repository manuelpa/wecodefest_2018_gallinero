import Vue from 'vue';

// UTILIDADES
import Router from 'vue-router';
import VueMeteorTracker from 'vue-meteor-tracker';
import globalFilters from '../../lib/globalFilters';
import globalMethods from '../../lib/globalMethods';

// COMPONENTES
import App from '../../ui/App.vue';
import AppComponents from '../../ui/components/appComponents';

// CONFIGURACIÓN
Vue.use(Router);
Vue.use(VueMeteorTracker);

AppComponents.forEach(({ name, module }) => { Vue.component(name, module); });

Vue.mixin({
  data() {
    return { };
  },
  methods: globalMethods,
  filters: globalFilters,
  computed: {
    allSubsReady() {
      return Object.values(this.$subReady || {})
        .reduce((accumulator, sub) => accumulator && sub, true);
    },
  },
});


function createApp() {
  import router from '../routes/config';
  import interceptors from '../routes/interceptors';

  interceptors.forEach((interceptor) => {
    router.beforeEach(interceptor);
  });

  return {
    app: new Vue({
      el: '#app',
      router,
      ...App,
    }),
  };
}

export default createApp;
